//
//  SYNPageViewController.swift
//
//
//  Created by Daniel Rutkovský on 03/04/17.
//  Copyright © 2017 Daniel Rutkovský All rights reserved.
//

import Stevia
import UIKit

/// A page view controller that displays nubmer of 
/// dots with provide active and inactive images
///
open class SYNPageControll: UIView {

    // MARK: - Views
    private let activeImage: UIImage?
    private let inactiveImage: UIImage?
    private var views: [UIImageView] = []

    // MARK: - Variables

    /// Index of the page that is currently selected
    ///
    /// Changes of this variable will change the currently selected dot
    ///
    public var currentPage: Int {
        didSet {
            updateDots()
        }
    }

    /// Number of dots that are presented
    ///
    /// Changes of this varible will change the number of displayed dots
    ///
    public var numberOfPages: Int {
        didSet {
            layoutViews()
        }
    }

    // MARK: - Init
    /// Public initializer
    ///
    /// - Parameters:
    ///   - activeImage: Image that will be used as an active dot
    ///   - inactiveImage: Image that will be used as an inactive dot
    public init(activeImage: UIImage?, inactiveImage: UIImage?) {
        self.activeImage = activeImage
        self.inactiveImage = inactiveImage
        self.currentPage = 0
        self.numberOfPages = 0
        super.init(frame: .zero)
    }

    /// NOT IMPLEMENTED
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private
    private func layoutViews() {
        views.forEach { (view) in
            view.removeFromSuperview()
        }
        views.removeAll()
        for _ in 0..<numberOfPages {
            views.append(UIImageView(image: inactiveImage, highlightedImage: activeImage))
        }
        var previousView: UIView?
        views.forEach { (view) in
            sv(view)
            view.contentMode = .center
            view.top(0).bottom(0).heightEqualsWidth()
            if let previousView = previousView {
                previousView - 10 - view
            } else {
                |view
            }
            previousView = view
        }
        if let previousView = previousView {
            previousView|
        }
    }

    private func updateDots() {
        for i in 0..<numberOfPages {
            views[i].isHighlighted = i == currentPage
        }
    }
}
