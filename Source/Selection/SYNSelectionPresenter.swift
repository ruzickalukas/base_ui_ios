//
//  SelectionPresenter.swift
//  Expensa
//
//  Created by Dominik on 28/09/2017.
//  Copyright © 2017 SYNETECH s.r.o. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import SYNBase

public protocol SYNSelectionPresenterProtocol {
    func itemDidSelect(item: SYNSelectionItem)
    func didCancel()

    var title: String { get }
    var array: Driver<[SYNSelectionItem]> { get }
}

public class SYNSelectionPresenter {

    public var view: SYNSelectionViewProtocol?

    typealias RouterProtocol = SYNSelectionRouterProtocol
    fileprivate var router: RouterProtocol

    public var title: String
    public var array: Driver<[SYNSelectionItem]>

    init(router: RouterProtocol, title: String, array: Observable<[SYNSelectionItem]>) {
        self.router = router
        self.title = title
        self.array = array.asDriver(onErrorJustReturn: [])
    }
}

extension SYNSelectionPresenter: SYNSelectionPresenterProtocol {

    public func itemDidSelect(item: SYNSelectionItem) {
        router.didSelectItem(item: item)
    }

    public func didCancel() {
        router.didCancelRouting()
    }
}
