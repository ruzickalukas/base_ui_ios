//
//  SYNPageViewController.swift
//
//
//  Created by Daniel Rutkovský on 03/04/17.
//  Copyright © 2017 Daniel Rutkovský All rights reserved.
//

import Stevia
import UIKit

public protocol SYNPageViewControllerDelegate: class {
    func prepareAnimationForTransition(fromVC: UIViewController?, toVC: UIViewController?)
    func didProceedWithTransition(progress: Double)
    func didChangePage(fromVC: UIViewController?, toVC: UIViewController?, index: Int)
}

/// Documentation
open class SYNPageViewController: UIViewController {

    // MARK: - Views
    private let pageViewController = UIPageViewController(transitionStyle: .scroll,
                                                          navigationOrientation: .horizontal,
                                                          options: nil)

    // MARK: - Variables

    fileprivate var currentPage: UIViewController?
    fileprivate var pages: [UIViewController] = []
    public weak var delegate: SYNPageViewControllerDelegate?
    public var isScrollingEnabled: Bool = true {
        didSet {
            pageViewController.dataSource = isScrollingEnabled ? self : nil
        }
    }
    public var currentPageIndex: Int {
        if let currentPage = currentPage {
            return pages.index(of: currentPage) ?? 0
        }
        return 0
    }
    public var pageCount: Int {
        return pages.count
    }

    // MARK: - Init

    public convenience init() {
        self.init(pages: [])
    }

    /// Initializer that provides the ability to init the scrollview with it's contents
    ///
    /// - Parameter pages: List of views that should be displayed
    public init(pages: [UIViewController]) {
        super.init(nibName: nil, bundle: nil)
        self.pages.insert(contentsOf: pages, at: 0)
        currentPage = pages.first
    }

    /// NOT IMPLEMENTED -> Do not use this function
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Prepare Layout

    private func render() {
        addChildViewController(pageViewController)
        self.view.sv(pageViewController.view)
        didMove(toParentViewController: pageViewController)
        pageViewController.view.fillContainer()
        pageViewController.dataSource = self
        pageViewController.delegate = self
        for view in pageViewController.view.subviews {
            if let view = view as? UIScrollView {
                view.delegate = self
                break
            }
        }
    }

    // MARK: - Life cycle

    override open func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        render()
    }

    // MARK: - Public
    open func showNextPage() {
        if let currentPage = currentPage,
            let controller = pageViewController(pageViewController, viewControllerAfter: currentPage) {
            pageViewController(self.pageViewController, willTransitionTo: [controller])
            pageViewController.setViewControllers([controller],
                                                  direction: .forward,
                                                  animated: true,
                                                  completion: { [unowned self] bool in
                                                    self.pageViewController(self.pageViewController,
                                                                            didFinishAnimating: bool,
                                                                            previousViewControllers: [currentPage],
                                                                            transitionCompleted: true)
            })
        }
    }

    open func showPreviousPage() {
        if let currentPage = currentPage,
            let controller = pageViewController(pageViewController, viewControllerBefore: currentPage) {
            pageViewController(self.pageViewController, willTransitionTo: [controller])
            pageViewController.setViewControllers([controller],
                                                  direction: .reverse,
                                                  animated: true,
                                                  completion: { [unowned self] bool in
                                                    self.pageViewController(self.pageViewController,
                                                                            didFinishAnimating: bool,
                                                                            previousViewControllers: [currentPage],
                                                                            transitionCompleted: true)
            })
        }
    }

    /// Adds a new page to the back of the list of pages.
    /// It displays the page directly when no pages were present.
    ///
    /// - Parameter page: A page to be added
    public func add(page: UIViewController) {
        if currentPage == nil {
            currentPage = page
        }
        if pageViewController.viewControllers == nil || pageViewController.viewControllers!.isEmpty {
            pageViewController.setViewControllers([page], direction: .forward, animated: false, completion: nil)
        }
        pages.append(page)
    }

    /// Clear all pages from current pageViewController and set new ones.
    /// First page will be shown imediatelly
    public func setPages(pages: [UIViewController]) {
        if let first = pages.first {
            currentPage = first
            pageViewController.setViewControllers([first], direction: .forward, animated: false, completion: nil)
        }
        self.pages = pages
    }

    // MARK: - Private

    private func setupConstraintsForPage(page: UIView) {
        page.height(self.view.bounds.height).width(self.view.bounds.width)
    }

}

extension SYNPageViewController: UIPageViewControllerDataSource {

    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let indexOfCurrent = pages.index(of: viewController), indexOfCurrent < pages.count - 1 else {
            return nil
        }
        let nextVC = pages[indexOfCurrent + 1]
        return nextVC
    }

    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let indexOfCurrent = pages.index(of: viewController), indexOfCurrent > 0 else {
            return nil
        }
        let nextVC = pages[indexOfCurrent - 1]
        return nextVC
    }
}

extension SYNPageViewController: UIPageViewControllerDelegate {

    public func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        delegate?.prepareAnimationForTransition(fromVC: currentPage, toVC: pendingViewControllers.first)
    }

    public func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if let controller = pageViewController.viewControllers?.first {
            currentPage = controller
            delegate?.didChangePage(fromVC: previousViewControllers.first,
                                    toVC: currentPage,
                                    index: pages.index(of: controller) ?? 0)
        }
    }
}

extension SYNPageViewController: UIScrollViewDelegate {

    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.x
        let currentScreen = offset / scrollView.frame.width
        let screenOffset = offset.truncatingRemainder(dividingBy: scrollView.frame.width)
        if let old = currentPage, let oldPageIndex = pages.index(of: old) {
            if CGFloat(oldPageIndex) > currentScreen {
                guard oldPageIndex > 0 else {
                    return
                }
                let progress = screenOffset / scrollView.frame.width
                delegate?.didProceedWithTransition(progress: Double(1 - progress))
            } else if CGFloat(oldPageIndex) < currentScreen {
                guard oldPageIndex < pages.count - 1 else {
                    return
                }
                let progress = screenOffset / scrollView.frame.width
                delegate?.didProceedWithTransition(progress: progress == 0 ? 1 : Double(progress))
            }
        }
    }
}
