//
//  SYNInputViewWithTitle.swift
//  
//
//  Created by Daniel Rutkovsky on 03/04/2017.
//
//

import Stevia
import UIKit

/// View that provides unified layout of an input with title and bottom line
open class SYNInputViewWithTitle: UIView {

    // MARK: - Views
    public let textField = UITextField()
    public let titleLbl = UILabel()
    public let bottomLine = UIView()

    // MARK: - Variables
    fileprivate var shouldReturnCallback: (UITextField) -> Bool

    // MARK: - Init

    /// Creates an UIView With Title, input field and bottom line
    ///
    /// - Parameters:
    ///   - input: parameters of the textField (placeholder, font, color)
    ///   - title: parameters of the title (text, font, color)
    ///   - spacingTitleInput: spacing between the title and input field
    ///   - spacingInputLine: spacing between the input field and bottom line
    ///   - lineColor: color of the bottom line
    ///   - keyboardType: type of keyboard that should be presented. Defaults to `.default`
    ///   - isSecureTextEntry: should the input be secure? Defaults to `false`
    ///   - shouldReturnCallback: callback if the textfield should return -> controlls the keyboard
    public init(input: (placeholder: String, font: UIFont?, color: UIColor),
                title: (text: String, font: UIFont?, color: UIColor),
                leftSpacing: CGFloat = 0,
                rightSpacing: CGFloat = 0,
                spacingTitleInput: CGFloat = 10,
                spacingInputLine: CGFloat = 12,
                lineColor: UIColor = .white,
                keyboardType: UIKeyboardType = .default,
                isSecureTextEntry: Bool = false,
                shouldReturnCallback: @escaping (UITextField) -> Bool) {
        self.shouldReturnCallback = shouldReturnCallback
        super.init(frame: CGRect.zero)
        setupViews(input: input,
                   title: title,
                   lineColor: lineColor,
                   keyboardType: keyboardType,
                   isSecureTextEntry: isSecureTextEntry)
        layoutViews(spacingTitleInput: spacingTitleInput,
                    spacingInputLine: spacingInputLine,
                    leftSpacing: leftSpacing,
                    rightSpacing: rightSpacing)
    }

    /// NOT IMPLEMETNED
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Prepare Layout

    private func setupViews(input: (placeholder: String, font: UIFont?, color: UIColor),
                            title: (text: String, font: UIFont?, color: UIColor),
                            lineColor: UIColor,
                            keyboardType: UIKeyboardType,
                            isSecureTextEntry: Bool) {
        titleLbl.style { label in
            label.font = title.font
            label.textColor = title.color
            label.text = title.text
            label.accessibilityIdentifier = "titleLbl"
        }

        textField.style { txtField in
            txtField.textColor = input.color
            txtField.tintColor = input.color
            txtField.font = input.font
            txtField.placeholder = input.placeholder
            txtField.accessibilityIdentifier = "textField"

        }
        textField.delegate = self
        textField.keyboardType = keyboardType
        textField.isSecureTextEntry = isSecureTextEntry
        bottomLine.style { button in
            button.backgroundColor = lineColor
        }
    }

    private func layoutViews(spacingTitleInput: CGFloat,
                             spacingInputLine: CGFloat,
                             leftSpacing: CGFloat,
                             rightSpacing: CGFloat) {
        sv(titleLbl, textField, bottomLine)
        layout(0,
               |-leftSpacing - titleLbl - rightSpacing-| ~ 15,
               spacingTitleInput,
               |-leftSpacing - textField - rightSpacing-| ~ 30,
               spacingInputLine,
               |bottomLine| ~ 1,
               0)
    }
}

extension SYNInputViewWithTitle: UITextFieldDelegate {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return shouldReturnCallback(textField)
    }
}

extension SYNInputViewWithTitle {
    public func setTitle(_ title: String) {
        self.titleLbl.text = title
    }
}
