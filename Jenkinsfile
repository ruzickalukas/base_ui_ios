#!/usr/bin/env groovy
/**
 Jenkins Multi-branch Pipeline

 Prerequisites (env variables):

 Steps:

 1) Clean workspace
 2) Checkout following branches: dev, stage/*, master.
 3) Prepare configurations (signing, ...)
 4) Check code quality
 5) Generate changelog and build project according to branch name.
 Branch mapping to build types:
 - dev -> debug
 - stage/* -> stage
 - master -> release and store
 6) Archive builds for given build.
 7) Upload builds to FABRIC Beta
 8) Clean up

 READ BEFORE USE !

 Following methods must be approved in Jenkins settings

 method java.util.regex.MatchResult group int
 method java.util.regex.Matcher matches
 staticMethod org.codehaus.groovy.runtime.DefaultGroovyMethods capitalize java.lang.String
 */

node('xcode8') {
        // Enable pretty colors 😉
        wrap([$class: 'AnsiColorBuildWrapper']) {
            // Set old builds strategy
            // Webhook trigger
            properties([
                [
                    $class: 'BuildDiscarderProperty',
                    strategy: [
                        $class: 'LogRotator',
                        artifactDaysToKeepStr: '',
                        artifactNumToKeepStr: '',
                        daysToKeepStr: '30',
                        numToKeepStr: '30'
                        ]
                ]
            ])
            
            currentBuild.result = "SUCCESS"

            def err = null
            def buildTypes = []
            def filesToCleanUp = []
            String changelogPath = "${WORKSPACE}/build/releaseNotes.txt"

            stage('Clean Workspace') {
                deleteDir()
            }

            stage('Checkout') {
                // Specify whan shoudl be checkedout from GIT
                checkout scm
            }

            stage('Prepare') {
                sh "echo 'reporter: html' >> .swiftlint.yml"
            }

            stage('CheckQuality') {
                try {
                    sh "mkdir -p ${WORKSPACE}/build/reports"
                    sh "swiftlint > ${WORKSPACE}/build/reports/lintReport.html"
                } catch (caughtError) {
                    echo "ERROR: ${caughtError}"
                } finally {
                    // Archive reports in every case
                    archiveArtifacts artifacts: "**/build/reports/", fingerprint: false
                }
            }

            stage('Validate branch') {
                if (env.BRANCH_NAME != 'master') {
                    currentBuild.result = 'SUCCESS'
                    echo "Current branch does not have a tag => We can only build tags!"
                    throw new Exception("SUCCESS")
                }
                hasTag = sh (
                    script: "git describe --exact-match --tags",
                    returnStatus: true
                ) == 0
                if (!hasTag) {
                    currentBuild.result = 'SUCCESS'
                    echo "Current branch does not have a tag => We can only build tags!"
                    throw new Exception("SUCCESS")
                }
            }

            stage('Build') {
                sh "fastlane ios release"
            }
        }
}
